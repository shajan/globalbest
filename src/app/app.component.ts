import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HomePage } from './home/home.page';
import { LoginPage } from './login/login.page';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  rootPage:any;

  constructor(
    platform: Platform,
  ) {
    platform.ready().then(() => {
      this.rootPage = LoginPage;
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      // splashScreen.hide();
    });
  }
}
