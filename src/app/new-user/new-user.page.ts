import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.page.html',
  styleUrls: ['./new-user.page.scss'],
})
export class NewUserPage implements OnInit {

  newUserForm: FormGroup;
  errorMessage: string;

  constructor(
    private router: Router,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public formBuilder: FormBuilder,
    private api: ApiService
  ) {}

  ngOnInit() {
    //Email Validator and Password Validator as requested
    this.newUserForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('iamdemo@gmail.com', [Validators.required, Validators.pattern(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/)]),
      phone: new FormControl('', Validators.required)
    });
  }

  create() {
    this.api.post('users', this.newUserForm.value).subscribe(resp => {
      console.log('resp', resp);
      if (resp) {
        this.presentAlert('User has been created successfully!');
      }
    }, err => {
      console.log(err);

    })
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Success',
      message: message,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
