import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserItem } from '../models/user';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  users: UserItem;
  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.api.get('users').subscribe((data: UserItem) => {
      console.log('Data', data);
      this.users = data;
    })
  }

  newUser() {
    this.router.navigate(['/new-user']);
  }

}
