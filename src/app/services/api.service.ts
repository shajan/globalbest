import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    public http: HttpClient
  ) { }

  get(category: string) {
    return this.http.get(environment.wordpress.api_url + "/" + category)
  }

  post(category: string, data: any) {
    return this.http.post(environment.wordpress.api_url + "/" + category, data);
  }
}
