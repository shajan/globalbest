import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  errorMessage: string;

  constructor(
    private router: Router,
    public loadingController: LoadingController,
    public formBuilder: FormBuilder,
  ) {}

  ngOnInit() {
    //Email Validator and Password Validator as requested
    this.loginForm = this.formBuilder.group({
      username: new FormControl('iamdemo@gmail.com', [Validators.required, Validators.pattern(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/)]),
      password: new FormControl('abcAA111', [
        Validators.minLength(8),
        Validators.maxLength(12),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
     ])
    });
  }

  async doLogin() {
    await this.router.navigate(['/home']);
  }
}
